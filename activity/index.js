// Copy the index.html and index.js to the activity folder.

// Listen to an event when the last name's input is changed.

// Instead of anonymous functions for each of the event listener:
// Create a function that will update the span's contents based on the value of the first and last name input fields.
// Instruct the event listeners to use the created function.

document.querySelector('#txt-first-name')
document.querySelector('#txt-last-name')
document.querySelector('#span-full-name')


const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
const txtFullName = document.querySelector('#txt-first-name', '#txt-last-name');


txtFirstName.addEventListener('keyup', (event) => {
  spanFullName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup', (event) => {
  spanFullName.innerHTML = txtLastName.value;
})

txtFullName.addEventListener('keyup', (event) => {
  spanFullName.innerHTML = txtFullName.value;
  event.target.value;
})

// txtFullName.addEventListener('keyup', (event) => {
//   spanFullName.innerHTML = txtFirstName.value;
//   spanFullName.innerHTML = txtLastName.value;
// })







