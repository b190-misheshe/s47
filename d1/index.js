// Using DOM
/*


*/
// document.getElementById('txt-first-name');
// document.getElementByClassName('txt-first-name');
// document.getElementByClassName('input');

document.querySelector('#txt-first-name')


// Event Listeners
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
// to perform an action when an event triggers, first you have to listen to it

txtFirstName.addEventListener('keypress', (event) => {
  spanFullName.innerHTML = txtFirstName.value;
})

// * event sample
// keyup
// keypress

txtFirstName.addEventListener('keyup', (event) => {
  // console.log(event.target);
  console.log(event.target.value);
});
